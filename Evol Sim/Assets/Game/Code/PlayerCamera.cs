using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace VoxelMaster
{
	public class PlayerCamera : MonoBehaviour
	{
		public bool locked = true;
		
		public float speed = 10f;
		public VoxelTerrain terrain;
		
		public Rigidbody body;

		public new Camera camera;
		public Transform pivot;
		
		public MeshRenderer renderer;
		
		CapsuleCollider capsuleCollider;
		
		float pitch;
		float yaw;
		
		float camDistance = -1f;
			
		void Start()
		{
			body = GetComponent<Rigidbody>();
			capsuleCollider = GetComponent<CapsuleCollider>();
		}

		void Update()
		{

			DoMouse();
			DoInput();
		}
		
		void FixedUpdate()
		{
			DoMovement();
		}
		
		void DoMouse()
		{
			bool finalLocked = (Input.GetKey(KeyCode.Tab) ? false : locked);
			Cursor.lockState = (finalLocked ? CursorLockMode.Locked : CursorLockMode.None);
			Cursor.visible = !finalLocked;
			
			
			RaycastHit hit;
			Ray ray = new Ray(pivot.transform.position, -camera.transform.forward);
			float hitDistance = camDistance;
			if(Physics.Raycast(ray, out hit, camDistance, ~(1 << gameObject.layer)))
			{
				hitDistance = hit.distance;
			}
			
			camera.transform.localPosition = -Vector3.forward * hitDistance;

			yaw += Input.GetAxis("Mouse Y");
			pitch += Input.GetAxis("Mouse X");	
			yaw = Mathf.Clamp(yaw, -90, 90);
			
			transform.localRotation = Quaternion.Euler(new Vector3(0, pitch, 0));
			pivot.localRotation = Quaternion.Euler(new Vector3(-yaw, 0, 0));
		}

		void DoMovement()
		{
			float forwardVelocity = speed * Input.GetAxis("Vertical");
			float lateralVelocity = speed * Input.GetAxis("Horizontal");
			Vector3 movement = transform.forward * forwardVelocity + transform.right * lateralVelocity;	
			float prevY = body.velocity.y;
			
			Vector3 velocity = Vector3.MoveTowards(body.velocity, movement, 0.3f);
			velocity.y = prevY;
			
			body.velocity = velocity;
			
			
			Vector3 rayPos = body.position - 0.1f * capsuleCollider.height * Vector3.up;
			Debug.DrawRay(rayPos, Vector3.down, Color.red);
			
			RaycastHit hit;
			Ray ray = new Ray(rayPos, Vector3.down);	
			
			Physics.Raycast(ray, out hit, capsuleCollider.height / 2f, ~(1 << gameObject.layer));
			
			if(Input.GetKeyDown(KeyCode.Space) && hit.collider != null)
			{
				body.velocity = Vector3.MoveTowards(body.velocity, Vector3.up * 7.0f, 5);
			}
		}

		void DoInput()
		{
			if (Input.GetMouseButtonDown(0)) // Destroy block
			{
				Ray r = camera.ViewportPointToRay(new Vector2(0.5f, 0.5f));
				RaycastHit hit;
				Physics.Raycast(r, out hit, Mathf.Max(10, camDistance), ~(1 << gameObject.layer));
				RemoveBlock(hit);
			}
			else if (Input.GetMouseButtonDown(1)) // Add stone block
			{
				Ray r = camera.ViewportPointToRay(new Vector2(0.5f, 0.5f));
				RaycastHit hit;
				Physics.Raycast(r, out hit, Mathf.Max(10, camDistance), ~(1 << gameObject.layer));
				PlaceBlock(1, hit);
			}
			
			if(Input.GetKeyDown(KeyCode.Z))
			{
				Ray r = camera.ViewportPointToRay(new Vector2(0.5f, 0.5f));
				RaycastHit hit;
				Physics.Raycast(r, out hit, Mathf.Max(10, camDistance), ~(1 << gameObject.layer));
				
				terrain.Sphere(hit.point + hit.normal * 0.5f, 2, 1);
				terrain.FastRefresh();
			}
			
			if(Input.GetKeyDown(KeyCode.V))
			{
				Debug.Log("Saved");
				terrain.SaveWorld("world");
			}
			
			float scroll = Input.GetAxis("Mouse ScrollWheel") * 10;
			if(scroll != 0)
			{
				camDistance += scroll;
				if(camDistance < 0.2f)
				{
					camDistance = 0;
					renderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
				}else 
				{
					renderer.shadowCastingMode = ShadowCastingMode.On;	
				}
			}
		}
		
		void PlaceBlock(int blockID, RaycastHit hit)
		{
			if (hit.collider != null)
			{
				Vector3 final = hit.point + (hit.normal * 0.5f);
				terrain.SetBlockID(final, 1);
				terrain.FastRefresh();
			}
		}
		
		void RemoveBlock(RaycastHit hit)
		{
			if (hit.collider != null)
			{
				Vector3 final = hit.point - (hit.normal * 0.5f);
				terrain.RemoveBlockAt(final);
				terrain.FastRefresh();
			}
		}
		
		void OnGUI()
		{
			string xDisp = "X: " + transform.localPosition.x;
			string yDisp = "Y: " + transform.localPosition.y;
			string zDisp = "Z: " + transform.localPosition.z;
			
			GUI.Label(new Rect(0, 0, 60, 60), xDisp);
			GUI.Label(new Rect(0, 60, 60, 60), yDisp);
			GUI.Label(new Rect(0, 120, 60, 60), zDisp);
		}
	}
}