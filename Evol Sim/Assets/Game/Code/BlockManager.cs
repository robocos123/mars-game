using VoxelMaster;
public class BlockManager
{
	public const uint VIEW_DISTANCE = 256;
	public const uint SIZEX = (uint)(Chunk.SIZEX * (VIEW_DISTANCE + 1));
	public const uint SIZEZ = (uint)(Chunk.SIZEZ * (VIEW_DISTANCE + 1));
	public const uint SIZEY = Chunk.SIZEY;

	public const uint SIZEXY = SIZEX * SIZEY;
	public const uint BUFFERSIZE = SIZEX * SIZEY * SIZEZ;

	public const int ADDX = (int)SIZEY;
	public const int SUBX = -(int)SIZEY;
	public const int ADDY = 1;
	public const int SUBY = -1;
	public const int ADDZ = (int)SIZEXY;
	public const int SUBZ = -(int)SIZEXY;

	public Block[] Blocks;

	public BlockManager()
	{
		Blocks = new Block[BUFFERSIZE];
	}

	public uint Index(uint x, uint y, uint z)
	{
		uint modx = x % SIZEX;
		uint mody = y % SIZEY;
		uint modz = z % SIZEZ;

		return (modz * SIZEXY) + (modx * SIZEY) + mody;
	}
}