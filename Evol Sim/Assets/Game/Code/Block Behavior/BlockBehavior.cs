﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class BlockBehavior : MonoBehaviour 
{
	public short blockID;
	
	public Rigidbody body;
	public MeshRenderer renderer;
	public MeshFilter filter;
	
	public BlockBehavior() 
	{
		
	}
}
