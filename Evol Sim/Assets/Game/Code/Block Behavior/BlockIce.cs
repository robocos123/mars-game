using UnityEngine;
using System.Collections;

public class BlockIce : BlockBehavior 
{

	public float m_Height = 1f;
	public float m_Speed = 1f;

	public Vector3 m_InitialPosition;

	void Start () 
	{
		renderer = gameObject.AddComponent<MeshRenderer>();
		Material[] mats = renderer.sharedMaterials;
		mats[0] = Resources.Load("ice", typeof(Material)) as Material;
		renderer.sharedMaterial = mats[0];
		renderer.enabled = true;
	
		
		body = GetComponent<Rigidbody>();
		if (body == null) body = gameObject.AddComponent<Rigidbody>();
		body.position = transform.position;
		body.useGravity = true;
		body.freezeRotation = true;
	}

	void Update ()
	{
		
	}
}
