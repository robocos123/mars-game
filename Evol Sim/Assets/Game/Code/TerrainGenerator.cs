﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelMaster;

[RequireComponent (typeof(VoxelGeneration))]
public class TerrainGenerator : BaseGeneration {
	
	public VoxelGeneration generation;
	public float seed;
	
	public void Awake()
	{
		generation.SetGenerationAction(Generation);
	}
	
	short Generation(int x, int y, int z)
	{
		Random.InitState((int) GenerateSeed());
		float height = 0f;

		// Height data, regardless of biome
		float mountainContrib = VoxelGeneration.Remap(Mathf.PerlinNoise(x / 150f, z / 150f), 0.33f, 0.66f, 0, 1) * 40f;
		float desertContrib = 0f;
		float oceanContrib = 0f;
		float detailContrib = VoxelGeneration.Remap(Mathf.PerlinNoise(x / 20f, z / 20f), 0, 1, -1, 1) * 5f;
		
		// Biomes
		float detailMult = VoxelGeneration.Remap(Mathf.PerlinNoise(x / 30f, z / 30f), 0.33f, 0.66f, 0, 1);
		float mountainBiome = VoxelGeneration.Remap(Mathf.PerlinNoise(x / 100f, z / 100f), 0.33f, 0.66f, 0, 1);
		float desertBiome = VoxelGeneration.Remap(Mathf.PerlinNoise(x / 300f, z / 300f), 0.33f, 0.66f, 0, 1) * VoxelGeneration.Remap(Mathf.PerlinNoise(x / 25f, z / 25f), 0.33f, 0.66f, 0.95f, 1.05f);
		float oceanBiome = VoxelGeneration.Remap(Mathf.PerlinNoise(x / 500f, z / 500f), 0.33f, 0.66f, 0, 1);

		// Add biome contrib
		float mountainFinal = (mountainContrib * mountainBiome) + (detailContrib * detailMult) + 40;
		float desertFinal = (desertContrib * desertBiome) + (detailContrib * detailMult) + 40;
		float oceanFinal = (oceanContrib * oceanBiome) * 2;

		// Final contrib
		height = Mathf.Lerp(mountainFinal, desertFinal, desertBiome); // Decide between mountain biome or desert biome
		height = Mathf.Lerp(height, oceanFinal, oceanBiome); // Decide between the previous biome or ocean biome (aka ocean biome overrides all biomes)

		height = Mathf.Floor(height);

		if (y <= height && y >= 0)
		{
			if (y == height && height > 2) 
			{
				if (oceanBiome >= 0.75f && height < 16)
					return 1;
				else //mountain
					return oceanBiome >= 0.5f && height > 80 ? (short) 1 : (short) 0;
			}
			else if (y >= height - 6 && height > 6) //sand layer
			{
				return 0;
			}
			else if (y > 0) // Stone layer
			{
				return 0;
			}
			else // Bedrock layer
			{
				return 0;
			}
		}
		else // Else there shall be nothing!
		{
			return -1;
		}
	}
	
	void Reset()
	{
		generation = GetComponent<VoxelGeneration>();	
	}
	
	float GenerateSeed()
	{
		return 40;
	}
}