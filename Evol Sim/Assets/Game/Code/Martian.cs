	using UnityEngine;
	using VoxelMaster;
	
	public class Martian: MonoBehaviour
	{
		public MeshRenderer renderer;
		public MeshFilter filter;

		public Transform transform;
		public Rigidbody body;
		public CapsuleCollider collider;
		public Light sun;
		public VoxelTerrain terrain;


		public float scale = 0.1f;
		public float speed = 10, rotSpeed = 8;
		public float yaw, pitch;

	void Start()
	{
		gameObject.layer = LayerMask.NameToLayer("AILayer");
		transform = gameObject.GetComponent<Transform>();
		collider = gameObject.AddComponent<CapsuleCollider>();
		collider.gameObject.AddComponent<Rigidbody>();
		
		collider.center = new Vector3(0, 1/scale, 0);
		
		transform.localScale *= scale;
		collider.height = 1/scale;
		collider.radius = 1/scale;
		body = GetComponent<Rigidbody>();
		
		renderer = gameObject.AddComponent<MeshRenderer>();
		filter = gameObject.AddComponent<MeshFilter>();
		
		Material[] mats = renderer.sharedMaterials;
		mats[0] = Resources.Load("Materials/martian", typeof(Material)) as Material;
		renderer.sharedMaterial = mats[0];
		
		filter.mesh = Resources.Load("Models/martian", typeof(Mesh)) as Mesh;
		body.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		transform.localPosition = new Vector3(20, Random.Range(30, 300), Random.Range(0, 100));
	}


	void Update()
	{
		
	}

	void FixedUpdate()
	{
		Vector3 rayPos = body.position - Vector3.up / 20;
		rayPos.x = body.position.x;
		rayPos.z = body.position.z;
		
		RaycastHit hit;
		Ray ray = new Ray(rayPos, Vector3.down);
		
		Physics.Raycast(ray, out hit, collider.height / 2f, ~(1 << gameObject.layer));
		Debug.DrawRay(rayPos, Vector3.down, Color.red);
		
		OnGroundCollision(hit);
	}
	
	void OnGroundCollision(RaycastHit downHit)
	{
		if(downHit.collider != null)
		{
			GameObject player = GameObject.Find("Robot");
			if(player)
			{
				Transform p = player.transform;
				Vector3 dir = transform.position - p.position;
				if(dir.magnitude >= 5f)
				{
					body.rotation = Quaternion.Slerp(body.rotation, Quaternion.LookRotation(dir), rotSpeed);
					body.velocity = Vector3.MoveTowards(body.velocity, -body.transform.forward * speed, 6);
				}
			}
			
			if(Vector3.Cross(transform.forward, Vector3.up).magnitude != 0)
			{
				Vector3 rayPos = body.position - transform.forward / 10f;
				RaycastHit forwardHit;
				Ray ray = new Ray(rayPos, -transform.forward);
				
				Physics.Raycast(ray, out forwardHit, collider.height / 10f, ~(1 << gameObject.layer));
				Debug.DrawRay(rayPos, -transform.forward, Color.black);
				if(forwardHit.collider != null)
				{
					body.velocity = Vector3.MoveTowards(body.velocity, Vector3.up * 700.0f - transform.forward * 1.5f, 20f);
				}
			}
		}
	}
}