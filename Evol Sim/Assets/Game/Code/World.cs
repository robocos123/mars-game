using UnityEngine;
using VoxelMaster;

public class World : MonoBehaviour
{
	public Material material;
	public Quaternion quaternion;
	public Light sun;
	public Transform lightTrans;
	public VoxelTerrain terrain;
	public PlayerCamera player;
	
	public Martian[] martians;
	
	void Start()
	{
		Vector3 rand = Random.insideUnitSphere;
		quaternion = Quaternion.Lerp(Quaternion.identity, new Quaternion(rand.x, rand.y, rand.z, Mathf.Sqrt(1.0f - rand.sqrMagnitude)), 0.01f);
		terrain = GetComponent<VoxelTerrain>();
		SpawnMartians(5);
	}
	
	void Update()
	{
		UpdateTimeOfDay();
	}
	
	void FixedUpdate()
	{
		
	}
	
	void SpawnMartians(int numMartians)
	{
		martians = new Martian[numMartians];
		for(int i = 0; i < martians.Length; i++)
		{
			GameObject obj = new GameObject("Martian " + (martians.Length - i));
			martians[i] = obj.AddComponent<Martian>();
			martians[i].sun = sun;
			martians[i].terrain = terrain;
		}
	}

	void UpdateTimeOfDay()
	{
		if(Time.frameCount % 15 == 0)
		{
			lightTrans.rotation *= quaternion;
			sun.intensity = Mathf.Clamp(lightTrans.position.y * 2.5f, 0, 2.5f);
		}
	}
}