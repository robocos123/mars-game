﻿		// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)
		
		Shader "Starfield" {
		Properties {
		_Tint ("Tint Color", Color) = (.5, .5, .5, .5)
		[Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
		_Rotation ("Rotation", Range(0, 360)) = 0
		[NoScaleOffset] _FrontTex ("Front [+Z]   (HDR)", 2D) = "grey" {}
		[NoScaleOffset] _BackTex ("Back [-Z]   (HDR)", 2D) = "grey" {}
		[NoScaleOffset] _LeftTex ("Left [+X]   (HDR)", 2D) = "grey" {}
		[NoScaleOffset] _RightTex ("Right [-X]   (HDR)", 2D) = "grey" {}
		[NoScaleOffset] _UpTex ("Up [+Y]   (HDR)", 2D) = "grey" {}
		[NoScaleOffset] _DownTex ("Down [-Y]   (HDR)", 2D) = "grey" {}
		[NoScaleOffset] _SkyGradient ("SkyGradient", 2D) = "grey" {}
		[KeywordEnum(None, Simple, High Quality)] _SunDisk ("Sun", Int) = 2
		 _SunSize ("Sun Size", Range(0,1)) = 0.04
		 _SunSizeConvergence("Sun Size Convergence", Range(1,10)) = 5
		}

		SubShader {
		Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox" }
		Cull Off ZWrite Off

		CGINCLUDE
		#include "UnityCG.cginc"

		half4 _Tint;
		half _Exposure;
		float _Rotation;
		
		float3 UP = float3(0.0, 1.0, 0.0);
		
		half4 GetSkyColor(float zenith, float sunRadius, float timeOfDay, sampler2D _SkyGradient)
		{
			timeOfDay = timeOfDay * .5 + .5;
			zenith = zenith * .5 + .5;
			sunRadius = sunRadius * .5 + .5;
			return tex2D(_SkyGradient, float2(0, zenith)) 
				+ tex2D(_SkyGradient, float2(0.5, timeOfDay)) 
				+ tex2D(_SkyGradient, float2(0.45, sunRadius));
			return half4(timeOfDay, zenith, sunRadius, 0.0);
		}

		float3 RotateAroundYInDegrees (float3 vertex, float degrees)
		{
			float alpha = degrees * UNITY_PI / 180.0;
			float sina, cosa;
			sincos(alpha, sina, cosa);
			float2x2 m = float2x2(cosa, -sina, sina, cosa);
			return float3(mul(m, vertex.xz), vertex.y).xzy;
		}

		struct appdata_t {
			float4 vertex : POSITION;
			float2 texcoord : TEXCOORD0;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};
		struct v2f {
			float4 vertex : SV_POSITION;
			float3 world : NORMAL;
			float2 texcoord : TEXCOORD0;
			UNITY_VERTEX_OUTPUT_STEREO
		};
		v2f vert (appdata_t v)
		{
			v2f o;
			UNITY_SETUP_INSTANCE_ID(v);
			UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
			float3 rotated = RotateAroundYInDegrees(v.vertex, _Rotation);
			o.vertex = UnityObjectToClipPos(rotated);
			o.texcoord = v.texcoord;
			
			o.world = rotated;
			return o;
		}
		half4 skybox_frag (v2f i, sampler2D smp, sampler2D skyGradient)
		{
			float3 lightPos = _WorldSpaceLightPos0.xyz;
			float3 w = normalize(i.world);
			
			half4 skyColor = GetSkyColor(w.y, dot(lightPos, w), lightPos.y, skyGradient);
			
			half4 tex = tex2D (smp, i.texcoord);
			half4 c = skyColor + tex; //* _Tint.rgb * unity_ColorSpaceDouble.rgb;
			// c *= _Exposure;
			return c;
		}
		ENDCG

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			sampler2D _FrontTex;
			sampler2D _SkyGradient;
			half4 frag (v2f i) : SV_Target { return skybox_frag(i,_FrontTex, _SkyGradient); }
			ENDCG
		}
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			sampler2D _BackTex;
			sampler2D _SkyGradient;
			half4 frag (v2f i) : SV_Target { return skybox_frag(i,_BackTex, _SkyGradient); }
			ENDCG
		}
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			sampler2D _LeftTex;
			sampler2D _SkyGradient;
			half4 frag (v2f i) : SV_Target { return skybox_frag(i,_LeftTex, _SkyGradient); }
			ENDCG
		}
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			sampler2D _RightTex;
			sampler2D _SkyGradient;
			half4 frag (v2f i) : SV_Target { return skybox_frag(i,_RightTex, _SkyGradient); }
			ENDCG
		}
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			sampler2D _UpTex;
			sampler2D _SkyGradient;
			half4 frag (v2f i) : SV_Target { return skybox_frag(i,_UpTex, _SkyGradient); }
			ENDCG
		}
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			sampler2D _DownTex;
			sampler2D _SkyGradient;
			half4 frag (v2f i) : SV_Target { return skybox_frag(i,_DownTex, _SkyGradient); }
			ENDCG
		}
	}
}
