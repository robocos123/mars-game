﻿Shader "Custom/Ice" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_MeltY("Melt Y", Float) = 0.0
		_MeltDistance("Melt Distance", Float) = 1.0
		_MeltPower("Melt Curve", Range(1.0,10.0)) = 2.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows vertex:vert addshadow
		

		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};
		
	
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		half _MeltY;

		half _MeltDistance;

		// controls the shape of the melt(melt ^ power)
		half _MeltPower;

		float4 getNewVertPosition(float4 objectSpacePosition, float3 objectSpaceNormal)
		{
			float4 worldSpacePosition = mul(unity_ObjectToWorld, objectSpacePosition);
			float4 worldSpaceNormal = mul(unity_ObjectToWorld, float4(objectSpaceNormal,0));
			float melt = (worldSpacePosition.y - _MeltY) / _MeltDistance;
			melt = 1 - saturate(melt);
			melt = pow( melt, _MeltPower );
			worldSpacePosition.xz += worldSpaceNormal.xz * melt;
			return mul(unity_WorldToObject, worldSpacePosition );
		}

		appdata_full vert( inout appdata_full v )
		{
			float4 vertPosition = getNewVertPosition( v.vertex, v.normal );
			float4 bitangent = float4( cross( v.normal, v.tangent ), 0 );
			float vertOffset = 0.01;

			float4 v1 = getNewVertPosition( v.vertex + v.tangent * vertOffset, v.normal );
			float4 v2 = getNewVertPosition( v.vertex + bitangent * vertOffset, v.normal );

			float4 newTangent = v1 - vertPosition;
			float4 newBitangent = v2 - vertPosition;

			v.normal = cross( newTangent, newBitangent );
			v.vertex = vertPosition;
			return v;
		}

		void surf (Input IN, inout SurfaceOutputStandard ou) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			ou.Albedo = c.rgb;
			ou.Metallic = _Metallic;
			ou.Smoothness = _Glossiness;
			ou.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

